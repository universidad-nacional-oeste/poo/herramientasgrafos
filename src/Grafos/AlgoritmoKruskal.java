package Grafos;
public class AlgoritmoKruskal {

	static int INFINITO = 0xFFFF;
	private int[][] matriz;
	private int num_vertices;
	private int[] parent;
	
	// Vertice origen y numero de vertice/
	private Vertice[] vertices;
	
	public AlgoritmoKruskal(Grafos gp) {
		this.num_vertices = gp.getVertsVec().length;
		this.matriz = gp.getMatAdyPeso();
		this.vertices = gp.getVertsVec();
		parent = new int[this.num_vertices+1];
	}
	
	protected void recorrer() {
		int i=0, j=0, ne=1, minimo=0, a=0, u=0, b=0, v=0, minimo_costo=0;
		System.out.println("CAMINO MINIMO:\n");
		while(ne < this.num_vertices) {
			for(i=1, minimo = INFINITO; i<=this.num_vertices; i++) {
				for(j=1; j<=this.num_vertices; j++) {
					if(this.matriz[i-1][j-1]<minimo) {
						minimo = this.matriz[i-1][j-1];
						a = u = i;
						b = v = j;
					}
				}
			}
			u = find(u);
			v = find(v);
			if(uni(u, v)>0) {
				System.out.println((ne++)+" Pasada: Arista entre Vertice: "+this.vertices[a-1].nomVertice()+" y Vertice: "+this.vertices[b-1].nomVertice()+" Peso = "+ minimo);
				minimo_costo += minimo; 
			}
			this.matriz[a-1][b-1] = this.matriz[b-1][a-1] = INFINITO;
		}
		System.out.println("");
		System.out.println("Costo minimo: " + minimo_costo);
	}
	
	private int find(int i) {
		while(this.parent[i]>0) {
			i = this.parent[i];
		}
		return i;
	}
	
	private int uni(int i, int j) {
		if(i != j) {
			this.parent[j] = i;
			return 1;
		}
		return 0;
	}
	
}