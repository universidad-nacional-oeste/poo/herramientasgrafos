package Grafos;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class Archivero {
	public int[][] leerArchivo(String direccion) {
		Scanner leo=new Scanner(System.in);
		int[][] matrizAd = new int[0][0];
		
		try{
			leo=new Scanner(new File(direccion));
			int nodos=leo.nextInt();
			int tuneles=leo.nextInt();
			int puentes=leo.nextInt();
			
			matrizAd=new int[nodos][nodos];
			leo.nextLine();	
			
			for(int i=0; i<tuneles;i++) {
				int nodo1=leo.nextInt();
				int nodo2=leo.nextInt();
				matrizAd[nodo1-1][nodo2-1]=1;//1 significa tunel
				leo.nextLine();
			}	
			
			for(int k=0; k<puentes;k++) {
				int nodo1=leo.nextInt();
				int nodo2=leo.nextInt();
				matrizAd[nodo1-1][nodo2-1]=10;//10 significa puente
				if(leo.hasNextLine())leo.nextLine();
			}	
		}catch(Exception e){	
			System.out.println(e.getMessage());
		}finally{
			if(leo!=null) leo.close();
		}return matrizAd;	
	}
	
	public void escribirArchivo(int minPuentes) {
		PrintWriter escribo=new PrintWriter(System.out);
		
		try{
			escribo=new PrintWriter(new File("metro.out"));	
			
			escribo.print(minPuentes);

		}catch(Exception e){	
			System.out.println(e.getMessage());
		}finally{
			if(escribo!=null) escribo.close();
		}
	}
}

